#-------------------------------------------------
#
# Project created by QtCreator 2018-03-11T22:29:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = jgy
TEMPLATE = app
QT       += printsupport


SOURCES += main.cpp\
        mainwindow.cpp\
        qcustomplot.cpp


HEADERS  += mainwindow.h\
   qcustomplot.h

FORMS    += mainwindow.ui
