%{
#include <alloca.h>
#include <math.h>
#include <stdlib.h>
#include <stddef.h>
#include <ctype.h>
#define YYSTYPE double

double reg[99];
double ans;
char format[20];
%}
%token NUMBER  EOLN PIVAL LOG
%token PLUS MINUS DIV MUL POW OPENBRACKET CLOSEBRACKET UNARYMINUS SQRT;
%token SIN COS TAN ASIN ACOS ATAN ABC
%token  OPENREG CLOSEREG REGA ANS  ASSIGN
%left PLUS MINUS
%left MUL DIV
%left UNARYMINUS
%left LOG
%%
lines: 
       | lines EOLN
       | lines expr EOLN  { printf( format , (double) $2 ); ans=$2; }
;
expr: pow;

pow: add
       | pow POW add { $$ = pow($1,$3); }
       | SQRT OPENBRACKET expr CLOSEBRACKET { $$ = sqrt($3) ; }
;
add: mul
       | add PLUS mul { $$ = $1 + $3; }
       | add MINUS mul { $$ = $1 - $3; }
;
mul: unary
       | mul MUL unary { $$ = $1 * $3; }
       | mul DIV unary { $$ = $1 / $3; }
;
unary: assign
       | MINUS primary %prec UNARYMINUS { $$ = -$2; }
       | LOG unary { $$ = log($2); }
;
assign: primary
       | REGA OPENREG expr CLOSEREG ASSIGN primary { reg[(int)$3]=$6; $$=$6; }
       | REGA OPENREG expr CLOSEREG { $$=reg[(int)$3]; }
       | REGA
         { int i;
           for(i=0;i<100;i++)
             if (reg[i]!=0)
                printf("%02d = %.2f\n",i,reg[i]);
           $$=0;
         }
;
primary: NUMBER { $$ = $1; }
         | PIVAL { $$ = M_PI; }
         | OPENBRACKET expr CLOSEBRACKET { $$ = $2; }
         | ANS { $$ = ans; }
         |function
;

function:        SIN OPENBRACKET expr CLOSEBRACKET   { $$ = (cos($3)*tan($3)); }
               | COS OPENBRACKET expr CLOSEBRACKET { $$ = cos($3); }
               | TAN OPENBRACKET expr CLOSEBRACKET { $$ = tan($3); }
               | ASIN OPENBRACKET expr CLOSEBRACKET{ $$ = asin($3); }
               | ACOS OPENBRACKET expr CLOSEBRACKET{ $$ = acos($3); }
               | ATAN OPENBRACKET expr CLOSEBRACKET{ $$ = atan($3); }
               | ABC OPENBRACKET expr CLOSEBRACKET { $$ = abs($3);}
;

%%
#include <stdio.h>
#include <ctype.h>

char *progname;
double yylval;

main( argc, argv )
char *argv[];
{
  progname = argv[0];
  strcpy(format,"%g\n");
  yyparse();
}
yyerror( s )
char *s;
{
  warning( s , ( char * )0 );
  yyparse();
}
warning( s , t )
char *s , *t;
{
  fprintf( stderr ,"%s: %s\n" , progname , s );
  if ( t )
  fprintf( stderr , " %s\n" , t );
}
