%{
#define YYSTYPE double
#include "term1.tab.h"
#include <math.h>
extern double yylval;
%}

number [0-9.]
%%
[ \t] { ; }
log     return LOG;
sin     return SIN;
cos     return COS;
tan     return TAN;
asin    return ASIN;
acos    return ACOS;
atan    return ATAN;
reg     return REGA;
sqrt    return SQRT;
abc     return ABC;

ans     return ANS;
pi      return PIVAL;

{number}+ { sscanf( yytext, "%lf", &yylval ); return NUMBER ; }

"="     return ASSIGN;
"["     return OPENREG;
"]"     return CLOSEREG;
"+"     return PLUS;
"-"     return MINUS;
"~"     return UNARYMINUS;
"/"     return DIV;
"*"     return MUL;
"^"     return POW;
"("     return OPENBRACKET;
")"     return CLOSEBRACKET;
"\n"    return EOLN;
